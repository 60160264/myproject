const userService = {
  userList: [
    { id: 1, name: 'Lerpong', gender: 'M' },
    { id: 2, name: 'Prasob', gender: 'M' }
  ],
  lastId: 3,
  addUser (data) {
    data.id = this.lastId++
    this.userList.push(data)
  },
  updateUser (data) {
    const index = this.userList.findIndex(item => item.id === data.id)
    this.userList.splice(index, 1, data)
  },
  deleteUser (data) {
    const index = this.userList.findIndex(item => item.id === data.id)
    this.userList.splice(index, 1)
  },
  getUsers () {
    return [...this.userList]
  }
}

export default userService
