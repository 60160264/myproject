const userService = {
  userList: [
  ],
  lastId: 1,
  addUser (data) {
    data.id = this.lastId++
    this.userList.push(data)
  },
  updateUser (data) {
    const index = this.userList.findIndex(item => item.id === data.id)
    this.userList.splice(index, 1, data)
  },
  deleteUser (data) {
    const index = this.userList.findIndex(item => item.id === data.id)
    this.userList.splice(index, 1)
  },
  getUsers () {
    return [...this.userList]
  }
}

export default userService
